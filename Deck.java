import java.util.Random;

public class Deck {

    private Card[] cards;
    private int numberOfCards;
    private Random rng;
    private String[] suit = {"Hearts", "Diamonds", "Clubs", "Spades"};
    private int[] value = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

    public Deck() {
        rng = new Random();
        cards = new Card[52];
        int i = 0;

        for (String s : suit) {
            for (int v : value) {
                cards[i] = new Card(v, s);
                i++;
            }
        }
        numberOfCards = cards.length; // Initialize the number of cards
    }

    public int length() {
        return numberOfCards;
    }

    public Card drawTopCard() {
        if (numberOfCards <= 0) {
            return null; // Deck is empty
        }
        numberOfCards--;
        return cards[numberOfCards];
    }

    public String toString() {
	String result= "";
        for (int i = 0; i < numberOfCards; i++) {
            result += cards[i].toString() + "\n";
        }
        return result;
    }

    public void shuffle() {
        for (int i = 0; i < numberOfCards - 1; i++) {
            int randomIndex = i + rng.nextInt(numberOfCards - i);
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}
