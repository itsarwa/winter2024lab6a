public class Card{
	
	private String suit;
	private int value;
	
	public Card( int value, String suit){
		this.suit=suit;
		this.value=value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		String printValue = Integer.toString(value);
		if (value == 1){
			printValue= "Ace";
		}else if (value == 11){
			printValue= "Jack";
		}else if (value == 12){
			printValue = "Queen";
		}else if (value == 13){
			printValue = "King";
		}
		return printValue + " of " + suit;
	}
	
}