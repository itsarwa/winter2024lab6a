import java.util.Scanner;

public class LuckyCardGameApp {
    public static void main(String[] args) {
        Deck deck = new Deck();
        deck.shuffle();

        System.out.println("Welcome to the Lucky Card Game!");

        // Asking the user for input
        Scanner scanner = new Scanner(System.in);
        int cardsToRemove = -1;
        while (cardsToRemove < 0 || cardsToRemove > deck.length()) {
            System.out.print("How many cards do you wish to remove? ");
            cardsToRemove = scanner.nextInt();
        }

        // Printing initial number of cards
        System.out.println("Initial number of cards: " + deck.length());

        // Remove specified number of cards
        for (int i = 0; i < cardsToRemove; i++) {
            deck.drawTopCard();
        }

        // Print number of cards after removal
        System.out.println("Number of cards after removal: " + deck.length());

        // Shuffle the deck again
        deck.shuffle();

        // Print the whole deck
        System.out.println("Shuffled deck:");
        System.out.println(deck.toString());

	}
}
