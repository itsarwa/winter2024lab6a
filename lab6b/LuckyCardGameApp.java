public class LuckyCardGameApp {
    public static void main(String[] args) {
        GameManager manager = new GameManager(); 
        int totalPoints = 0; 
		 int roundNumber = 1; 

        System.out.println("Welcome to the Lucky Card Game!"); 

        // Game loop
        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
			System.out.println("Round " + roundNumber); // Print round number
            System.out.println(manager); // Print GameManager object

            int points = manager.calculatePoints(); // Calculate points
            totalPoints += points; // Update total points
            System.out.println("Points after Round " + roundNumber + ": " + points); // Print updated total points
			System.out.println("*******************************************************");
            manager.dealCards(); // Deal new cards to the user
			roundNumber++;
        }

        // Print final score and game result
        if (totalPoints >= 5) {
            System.out.println("Player wins with: " + totalPoints + "\n" + "Congratulations! You win the game!");
        } else {
            System.out.println("Sorry, you lose the game. Better luck next time!");
        }
    }
}

