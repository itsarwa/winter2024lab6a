public class GameManager {
    private Deck drawPile;
    private Card centerCard;
    private Card playerCard;

    public GameManager() {
        drawPile = new Deck();
        drawPile.shuffle();
        centerCard = drawPile.drawTopCard();
        playerCard = drawPile.drawTopCard();
    }

    public String toString() {
        String result = "---------------------------------------------------\n";
        result += "Center card: " + centerCard.toString() + "\n";
        result += "Player card: " + playerCard.toString() + "\n";
        result += "----------------------------------------------------";
        return result;
    }

    public void dealCards() {
        drawPile.shuffle(); // Shuffle the deck
        centerCard = drawPile.drawTopCard(); // Draw a card for the center
        playerCard = drawPile.drawTopCard(); // Draw a card for the player
    }

    public int getNumberOfCards() {
        return drawPile.length();
    }

    public int calculatePoints() {
        if (centerCard.getValue() == playerCard.getValue()) {
            return 4; // If the value of the two cards are the same
        } else if (centerCard.getSuit().equals(playerCard.getSuit())) {
            return 2; // If the suit of the two cards are the same
        } else {
            return -1; // If neither is the same
        }
    }
}
